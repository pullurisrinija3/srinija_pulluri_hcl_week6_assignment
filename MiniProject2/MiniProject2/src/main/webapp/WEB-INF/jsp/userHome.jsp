<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Home</title>

<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
</head>
<body>
	
	<div align="center">
		<h1>SURABI RESTAURANT</h1>
		<hr>
		<%
		Object loginResult = request.getAttribute("objLogInResult");
		Object name = session.getAttribute("objName");
		if (loginResult != null) {
			out.print(loginResult);
		}
		%>
		<hr>
		<div align="center">
		<a href="/user/userlogout"><button class="btn">Logout</button>
			<br></a>
	    </div>
	    <hr>
		<h2>Welcome Mr/Mrs.	<%=name%></h2>
		<a href="/menu/displayMenuForUser"><button class="btn1">Menu</button></a> <a
			href="/cart/userCart"><button class="btn1">My Cart</button></a> <br>
			<hr>
		<%
		Object bill = request.getAttribute("objOrder");
		if (bill != null) {
			out.print("<h3>" + bill + "</h3>");
		%>
		<a href="/order/getBill"><button class="btn1">View Bill</button></a>
		<%
		}
		Object amount = request.getAttribute("objAmount");
		if (amount != null) {
		out.print("<h3>" + amount + "</h3>");
		}
		%>
	
</body>
</html>