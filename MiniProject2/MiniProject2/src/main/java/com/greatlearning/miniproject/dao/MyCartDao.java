package com.greatlearning.miniproject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.greatlearning.miniproject.bean.*;

@Repository
public interface MyCartDao extends JpaRepository<MyCart, CompositeKeyForCart> {

	@Query("select c from MyCart c where c.key.email=:email")
	public List<MyCart> getMyCart(@Param("email") String email);

	@Query("select sum(c.totalPrice) from MyCart c where c.key.email=:email")
	public float getMyTotal(@Param("email") String email);

	@Transactional
	@Modifying
	@Query("Delete from MyCart c where c.key.email=:email")
	public void deleteAll(@Param("email") String email);

}
