package com.greatlearning.miniproject.controller;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.service.*;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminService adminService;

	@GetMapping(value = "/")
	public String openIndexPage() {
		return "index";
	}

	@GetMapping(value = "adminLoginPage")
	public String openAdminLogIn() {
		return "adminLogin";
	}
	@PostMapping(value = "adminLogin")
	public String openAdminHomePage(HttpServletRequest request) {

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String loginResult = adminService.verifyPassword(new Admin(email, password));

		if (loginResult.contains("Successful")) {
			request.setAttribute("objLogInResult", loginResult);
			return "adminHome";
		} else if (loginResult.contains("Failed")) {
			request.setAttribute("objLogInResult", loginResult);
			return "adminLogin";
		}
		return "index";

	}

	@GetMapping(value = "adminLogout")
	public String logout(HttpSession hs) {
		hs.invalidate();
		return "index";
	}

	@GetMapping(value = "adminHome")
	public String openAdminHome() {
		return "adminHome";
	}
}
