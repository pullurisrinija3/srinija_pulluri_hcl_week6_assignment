create database week6Assignment_srinija;

use week6Assignment_srinija;

create table MoviesComing(id int primary key,title varchar(30),year int,genres varchar(50),ratings varchar(50),poster varchar(150),contentRating varchar(30),duration varchar(20),releaseDate date,averageRating int,originalTitle varchar(20),storyline varchar(150),actors varchar(100),imdbRating float,posterurl varchar(164));


insert into MoviesComing values(1,"Game Night",2018,'Action, Crime,Drama','2,9,5,9,7,7,4,5,5,6,5,3,7',"MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM.jpg","11","PT100M",'2018-02-28',0,"","A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.",'Bruce Willis, Vincent,Elisabeth Shue',4.4,"https://images-na.ssl-images amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@.jpg");

insert into MoviesComing values(2,"Area X: Annihilation",2018,'Adventure,Drama, Fantasy','2,6,2,3,9,5,3,10,6,7,3,1,6',"MV5BMTk2Mjc2NzYxNl5BMl5BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg","R","",'2018-02-23',0,"Annihilation","A biologist's husband disappears. She puts her name forward for an expedition into an environmental disaster zone",'Tessa Thompson,Jennifer Jason Leigh, Natalie Portman',0,"https://images-na.ssl-images-amazon.com/images/M/MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg");

insert into MoviesComing values(3,"Hannah",2017,'Drama,action,comics','1,10,4,9,10,1,9,3,4,4,3',"MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","","PT95M",'2018-02-24',0,"","Intimate portrait of a woman drifting between reality and denial when she is left alone.",'Charlotte Rampling,Wilms, Van Vyve',6.5,"https://images-na.ssl-images-amazon.com/images/M/MV5BMTk2Mjc2NzYxNl5BMl5BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg");

insert into MoviesComing values(4,"TheLodgers",2017,'Drama,Horror,Romance','7,5,1,5,4,10,2,7,6,4,10,5',"MV5BM2FhM2E1MTktMDYwZi00ODA1LWI0YTYtN2NjZjM3ODFjYmU5XkEyXkFqcGdeQXVyMjY1ODQ3NTA@._V1_SY500_CR0,0,337,500_AL_.jpg","R","PT92M",'2018-03-09',0,"","1920, rural Ireland. Anglo Irish twins Rachel and Edward share a strange existence.",'Charlotte Vega,David Bradley,Moe Dunford',5.8,"https://images-na.ssl-images-amazon.com/images/M/MV5BM2FhM2E1MTktMDYwZi00ODA1LWI0YTYtN2NjZjM3ODFjYmU5XkEyXkFqcGdeQXVyMjY1ODQ3NTA@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into MoviesComing values
(5,"BeastofBurden",2018,'Action,Crime,Drama','9,5,5,5,5,6,10,6,6,9,6',"MV5BMjEyNTM3MDQ2NV5BMl5BanBnXkFtZTgwMDI5Nzk1NDM@._V1_SY500_CR0,0,336,500_AL_.jpg","R","",'2017-03-12',0,"","Sean Haggerty only has an hour to deliver his illegal cargo. An hour to reassure a drug cartel, a hitman, and the DEA that nothing is wrong.",'Daniel Radcliffe, Grace Gummer,Pablo Schreiber',0,"https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyNTM3MDQ2NV5BMl5BanBnXkFtZTgwMDI5Nzk1NDM@._V1_SY500_CR0,0,336,500_AL_.jpg");


select * from MoviesComing;


create table MoviesInTheatres(id int primary key,title varchar(30),year int,genres varchar(50),ratings varchar(50),poster varchar(150),contentRating varchar(30),duration varchar(20),releaseDate date,averageRating int,originalTitle varchar(20),storyline varchar(1230),actors varchar(50),imdbRating float,posterurl varchar(164));


insert into MoviesInTheatres values
(1,"BlackPanther",2018,'Action,Adventure,scfi','4,1,9,6,2,10,6,5,1,7,4',"MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY500_CR0,0,337,500_AL_.jpg","15","PT134M",'2018-02-14',0,"","After the events of Captain America: Civil War, King T'Challa returns home to the reclusive, technologically advanced African nation",'Chadwick Boseman,Michael B.Jordan,Lupita Nyongo',7.0,"https://images-na.ssl-images-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into MoviesInTheatres values
(2,"GrottmannenDug",2018,'Animation,Adventure,Comedy','7,9,6,1,3,8,3,8,7,6,10',"MV5BYWMxYWVjNzAtMTY0YS00YWQyLWExMGItMjUxODkwYzQyNzMwXkEyXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY500_CR0,0,328,500_AL_.jpg","","PT89M",'2018-03-23',0,"","Set at the dawn of time, when prehistoric creatures and woolly mammoths roamed the earth, Early Man tells the story of Dug.",'Tom Hiddleston, Eddie Redmayne,Maisie Williams',6.3,"https://images-na.ssl-images-amazon.com/images/M/MV5BYWMxYWVjNzAtMTY0YS00YWQyLWExMGItMjUxODkwYzQyNzMwXkEyXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY500_CR0,0,328,500_AL_.jpg");

insert into MoviesInTheatres values
(3,"Aiyaary",2018,'Action,Crime,Drama','1,2,8,10,3,8,8,1,6,8',"MV5BMjI1NTk0NTc1OV5BMl5BanBnXkFtZTgwNTMwMTE4NDM@._V1_SY500_CR0,0,281,500_AL_.jpg","","PT157M",'2018-02-16',0,"","Two officers with patriotic hearts suddenly have a fallout. The mentor, Colonel Abhay Singh has complete faith in the country's system.",'Anupam Kher,Sidharth Malhotra,Naseeruddin Shah',0,"https://images-na.ssl-images-amazon.com/images/M/MV5BMjI1NTk0NTc1OV5BMl5BanBnXkFtZTgwNTMwMTE4NDM@._V1_SY500_CR0,0,281,500_AL_.jpg");

insert into MoviesInTheatres values
(4,"Samson",2018,'Action,Drama,comics','9,6,5,7,5,8,9,4,10',"MV5BYThiMjg4ZDAtNjk5ZS00ZTUxLThmM2ItMGI0ZTE1NjRhNWNmXkEyXkFqcGdeQXVyNTQ3MjE4NTU@._V1_SY500_CR0,0,334,500_AL_.jpg","PG-13","",'2018-02-16',0,"","A Hebrew with an unusual gift of strength must respond properly to the call of God on his life in order to lead his people out of enslavement.",'Jackson Rathbone,Billy Zane,Taylor James',5.0,"https://images-na.ssl-images-amazon.com/images/M/MV5BYThiMjg4ZDAtNjk5ZS00ZTUxLThmM2ItMGI0ZTE1NjRhNWNmXkEyXkFqcGdeQXVyNTQ3MjE4NTU@._V1_SY500_CR0,0,334,500_AL_.jpg");

insert into MoviesInTheatres values
(5,"Loveless",2017,'Drama,action,comics','7,5,7,6,10,8,8,7,3',"MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg","R","PT127M",'2018-06-01',0,"","Still living under the same roof, the Moscow couple of Boris and Zhenya is in the terrible final stages of a bitter divorce",'Maryana Spivak,Aleksey Rozin,Matvey Novikov',7.8,"https://images-na.ssl-images-amazon.com/images/M/MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg");

select * from MoviesInTheatres;


create table TopRatedIndia(title varchar(30) primary key,year int,genres varchar(50),ratings varchar(50),poster varchar(150),contentRating varchar(30),duration varchar(20),releaseDate date,averageRating int,originalTitle varchar(20),storyline varchar(1230),actors varchar(50),imdbRating float,posterurl varchar(164));


insert into TopRatedIndia values
("anand",1971,'Drama,action,comics','4,1,9,6,2,10,6,5,1,7,4',"MV5BMjE0Mzk3OTk2NF5BMl5BanBnXkFtZTgwMTQ1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg","","PT122M",'1971-03-12',0,"","A melodramatic tale of a man with a terminal disease. The story begins with Dr Bhaksar winning a literary prize for his book",'Rajesh Khanna,Amitabh Bachchan,Sumita Sanyal',8.9,"https://images-na.ssl-images-amazon.com/images/M/MV5BMjE0Mzk3OTk2NF5BMl5BanBnXkFtZTgwMTQ1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg");

insert into TopRatedIndia values
("dangal",2016,'Action,Biography,Drama','7,9,6,1,3,8,3,8,7,6,10',"MV5BMTQ4MzQzMzM2Nl5BMl5BanBnXkFtZTgwMTQ1NzU3MDI@._V1_SY500_CR0,0,356,500_AL_.jpg","","PT161M",'2016-12-23',0,"","Biopic of Mahavir Singh Phogat, who taught wrestling to his daughters Babita Kumari and Geeta Phogat. Geeta Phogat was India's first female wrestler",'Aamir Khan,Sakshi Tanwar,Fatima Sana Shaikh',8.9,"https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ4MzQzMzM2Nl5BMl5BanBnXkFtZTgwMTQ1NzU3MDI@._V1_SY500_CR0,0,356,500_AL_.jpg");

insert into  TopRatedIndia values
("Drishyam",2013,'Crime,Drama,Thriller','1,2,8,10,3,8,8,1,6,8',"MV5BYmY3MzYwMGUtOWMxYS00OGVhLWFjNmUtYzlkNGVmY2ZkMjA3XkEyXkFqcGdeQXVyMTExNDQ2MTI@._V1_SX330_CR0,0,330,432_AL_.jpg","","PT160M",'2013-12-19',0,"","Georgekutty (Mohanlal) is a cable TV network owner in a remote and hilly village in Kerala. He lives a happy life with his wife and 2 girls. ",'Mohanlal,Meena,Ansiba',0,"https://images-na.ssl-images-amazon.com/images/M/MV5BMjI1NTk0NTc1OV5BMl5BanBnXkFtZTgwNTMwMTE4NDM@._V1_SY500_CR0,0,281,500_AL_.jpg");

insert into TopRatedIndia values
("Nayakan",1987,'crime,Drama,action','9,6,5,7,5,8,9,4,10',"MV5BNTI2Zjc5ODMtNGE0NC00YjU5LTk0NjktZjU4ZDRlZDFkZWU0XkEyXkFqcGdeQXVyNjc5Mjg4Nzc@._V1_SY480_SX320_AL_.jpg","","PT145M",'1987-07-31',0,"","A small boy (Ratnavelu) from Tamilnadu sees his father, a labor leader, killed in cold blood by a policeman. He kills the policeman and runs away to the city of Bombay.",'Kamal Haasan,Saranya Ponvannan,Delhi Ganesh',8.9,"https://images-na.ssl-images-amazon.com/images/M/MV5BNTI2Zjc5ODMtNGE0NC00YjU5LTk0NjktZjU4ZDRlZDFkZWU0XkEyXkFqcGdeQXVyNjc5Mjg4Nzc@._V1_SY480_SX320_AL_.jpg");

insert into TopRatedIndia values
("Loveless",2017,'Drama,action,scifi','7,5,7,6,10,8,8,7,3',"MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg","R","PT127M",'2018-06-01',0,"Nelyubov","Still living under the same roof, the Moscow couple of Boris and Zhenya is in the terrible final stages of a bitter divorce",'Maryana Spivak,Aleksey Rozin,Matvey Novikov',7.8,"https://images-na.ssl-images-amazon.com/images/M/MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg");

select * from TopRatedIndia;


create table TopRatedMovies(title varchar(30) primary key,year int,genres varchar(50),ratings varchar(50),poster varchar(150),contentRating varchar(30),duration varchar(20),releaseDate date,averageRating int,originalTitle varchar(20),storyline varchar(1230),actors varchar(50),imdbRating float,posterurl varchar(164));


insert into TopRatedMovies values
("Baazigar",1993,'Crime,Drama,Musical','4,1,9,6,2,10,6,5,1,7,5',"MV5BMTc3OTE1MDAxNV5BMl5BanBnXkFtZTgwNjM1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg","","PT175M",'1993-11-12',0,"","Widowed Madan Chopra lives a very wealthy lifestyle with two daughters, Seema and Priya. His passion is car racing, and realizing that he is not young anymore",'Shah Rukh Khan,Rakhee Gulzar,Kajol',7.8,"https://images-na.ssl-images-amazon.com/images/M/MV5BMTc3OTE1MDAxNV5BMl5BanBnXkFtZTgwNjM1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg");

insert into TopRatedMovies values
("Drishyam",2008,'Action,Adventure,Biography','1,4,6,9,7,8,10,4,2',"MV5BZmYyNWRmNjUtODg5OC00YmRjLTkwYWMtMjY5NjA5ZjhkNDg3XkEyXkFqcGdeQXVyNDA1NTY2Nzk@._V1_SY500_CR0,0,333,500_AL_.jpg","","PT164M",'2016-05-06',0,"","A Sci-Fi family revenge drama happening between a scientist, his evil brother and the scientist's son, over a time travel gadget.",'Suriya,Samantha Ruth Prabhu,Nithya Menon',8.0,"https://images-na.ssl-images-amazon.com/images/M/MV5BZmYyNWRmNjUtODg5OC00YmRjLTkwYWMtMjY5NjA5ZjhkNDg3XkEyXkFqcGdeQXVyNDA1NTY2Nzk@._V1_SY500_CR0,0,333,500_AL_.jpg");

insert into  TopRatedMovies values
("Jodhaaakbar",2013,'Crime,Drama,Thriller','1,2,8,10,3,8,8,1,6,8',"MV5BMTI1ODQ2MDIxMl5BMl5BanBnXkFtZTcwNTc3Mzk1MQ@@._V1_SY361_SX250_AL_.jpg","","PT213M",'2009-10-02',0,"","In Mumbai, Sid Mehra is, in the words of his father, an arrogant, spoiled brat. He lives with a doting mother, subservient brother, and a father",'Ranbir Kapoor,Konkona Sen Sharma,Supriya Pathak',7.6,"https://images-na.ssl-images-amazon.com/images/M/MV5BMTg2NTk2MjU5NV5BMl5BanBnXkFtZTcwNTY0Nzg4Mg@@._V1_SY489_SX324_AL_.jpg");

insert into TopRatedMovies values
("wakeupsid",2009,'Comedy",Drama,Romance','9,8,7,6,5,5,3,6,10,2',"MV5BMTg2NTk2MjU5NV5BMl5BanBnXkFtZTcwNTY0Nzg4Mg@@._V1_SY489_SX324_AL_.jpg","","PT138M",'1987-07-31',0,"","A small boy (Ratnavelu) from Tamilnadu sees his father, a labor leader, killed in cold blood by a policeman. He kills the policeman and runs away to the city of Bombay.",'Kamal Haasan,Saranya Ponvannan,Delhi Ganesh',8.9,"https://images-na.ssl-images-amazon.com/images/M/MV5BNTI2Zjc5ODMtNGE0NC00YjU5LTk0NjktZjU4ZDRlZDFkZWU0XkEyXkFqcGdeQXVyNjc5Mjg4Nzc@._V1_SY480_SX320_AL_.jpg");

insert into TopRatedMovies values
("SaalaKhadoos",2016,'Drama,comics,genres','7,5,7,6,10,8,8,7,3',"MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg","R","PT127M",'2016-06-01',0,"Nelyubov","Still living under the same roof, the Moscow couple of Boris and Zhenya is in the terrible final stages of a bitter divorce",'Maryana Spivak,Aleksey Rozin,Matvey Novikov',7.8,"https://images-na.ssl-images-amazon.com/images/M/MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg");

select * from TopRatedMovies;

create table favourites(currentid int primary key,title varchar(20),posterurl varchar(150),year int,releasedate date,id int);

insert into favourites values(1,"Game night","https://images-na.ssl-images amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@.jpg",2018,'2018-02-28',1);

insert into favourites values(2,"area X:Annihilation","https://images-na.ssl-images-amazon.com/images/M/MV5BZmYyNWRmNjUtODg5OC00YmRjLTkwYWMtMjY5NjA5ZjhkNDg3XkEyXkFqcGdeQXVyNDA1NTY2Nzk.jpg",2018,'2018-02-23',2);

insert into favourites values(3,"Hannah","https://images-na.ssl-images-amazon.com/images/M/MV5BMTc3OTE1MDAxNV5BMl5BanBnXkFtZTgwNjM1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg",2017,'2018-01-24',3);

insert into favourites values(4,"Baazigar","https://images-na.ssl-images-amazon.com/images/M/MV5BMzU3ODQ3MzQ5Nl5BMl5BanBnXkFtZTgwMDQwMDIzNDM@._V1_SY500_CR0,0,338,500_AL_.jpg",2016,'2016-01-8',4);

insert into favourites values(5,"loveless","https://images-na.ssl-images-amazon.com/images/M/MV5BMTg2NTk2MjU5NV5BMl5BanBnXkFtZTcwNTY0Nzg4Mg@@._V1_SY489_SX324_AL_.jpg",2016,'2016-01-09',5);

select * from favourites;































