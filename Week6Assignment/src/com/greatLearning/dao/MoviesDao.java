package com.greatLearning.dao;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;
import com.greatLearning.bean.TopRatedMovies;
import com.greatLearning.resource.DbResource;
import com.greatLearning.bean.TopRatedIndia;
import com.greatLearning.bean.MoviesComing;
import com.greatLearning.bean.MoviesInTheatres;
import com.greatLearning.bean.Favourites;

public class MoviesDao {
	public List<MoviesComing> findAllMoviesComing() {
		List<MoviesComing> listOfMoviesComing = new ArrayList<>();
		try {
			//Class.forName("com.mysql.cj.jdbc.Driver");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6Assignment_srinija","root","Srinija123!");
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from MoviesComing");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				MoviesComing mc= new MoviesComing();
					mc.setId(rs.getInt(1));
					mc.setTitle(rs.getString(2));
					mc.setYear(rs.getInt(3));
					mc.setGenres(rs.getString(4));
					mc.setRatings(rs.getString(5));
					mc.setPoster(rs.getString(6));
					mc.setContentRating(rs.getString(7));
					mc.setDuration(rs.getString(8));
					mc.setReleaseDate(rs.getDate(9));
					mc.setAverageRating(rs.getInt(10));
					mc.setOriginalTitle(rs.getString(11));
					mc.setStoryline(rs.getString(12));
					mc.setActors(rs.getString(13));
					mc.setImdbRating(rs.getFloat(14));
					mc.setPosterurl(rs.getString(15));
					listOfMoviesComing.add(mc);
			}
			} catch (Exception e) {
				System.out.println("cannot retrieve"+e);
			}
		return listOfMoviesComing;
	}
	public List<MoviesInTheatres> findAllMoviesInTheatres() {
		List<MoviesInTheatres> listOfMoviesInTheatres = new ArrayList<>();
		try {
			
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from MoviesInTheatres");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					MoviesInTheatres mt= new MoviesInTheatres();
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getInt(3));
					mt.setGenres(rs.getString(4));
					mt.setRatings(rs.getString(5));
					mt.setPoster(rs.getString(6));
					mt.setContentRating(rs.getString(7));
					mt.setDuration(rs.getString(8));
					mt.setReleaseDate(rs.getDate(9));
					mt.setAverageRating(rs.getInt(10));
					mt.setOriginalTitle(rs.getString(11));
					mt.setStoryline(rs.getString(12));
					mt.setActors(rs.getString(13));
					mt.setImdbRating(rs.getFloat(11));
					mt.setPosterurl(rs.getString(12));
					listOfMoviesInTheatres.add(mt);
			}
			} catch (Exception e) {
				System.out.println("cannot retrieve "+e);
			}
		return listOfMoviesInTheatres;
	}
	public List<TopRatedIndia> findAllTopRatedIndia() {
		List<TopRatedIndia> listOfTopRatedIndia = new ArrayList<>();
		try {
			
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from TopRatedIndia");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedIndia tr= new TopRatedIndia();
					tr.setTitle(rs.getString(1));
					tr.setYear(rs.getInt(2));
					tr.setGenres(rs.getString(3));
					tr.setRatings(rs.getString(4));
					tr.setPoster(rs.getString(5));
					tr.setContentRating(rs.getString(6));
					tr.setDuration(rs.getString(7));
					tr.setReleaseDate(rs.getDate(8));
					tr.setAverageRating(rs.getInt(9));
					tr.setOriginalTitle(rs.getString(10));
					tr.setStoryline(rs.getString(11));
					tr.setActors(rs.getString(12));
					tr.setImdbRating(rs.getString(13));
					tr.setPosterurl(rs.getString(14));
					listOfTopRatedIndia.add(tr);
			}
			} catch (Exception e) {
				System.out.println("cannot retrieve "+e);
			}
		return listOfTopRatedIndia;
	}
	public List<TopRatedMovies> findAllTopRatedMovies() {
		List<TopRatedMovies> listOfTopRatedMovies = new ArrayList<>();
		try {
			
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from TopRatedMovies");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedMovies tm= new TopRatedMovies();
					tm.setTitle(rs.getString(1));
					tm.setYear(rs.getInt(2));
					tm.setGenres(rs.getString(3));
					tm.setRatings(rs.getString(4));
					tm.setPoster(rs.getString(5));
					tm.setContentRating(rs.getString(6));
					tm.setDuration(rs.getString(7));
					tm.setReleaseDate(rs.getDate(8));
					tm.setAverageRating(rs.getInt(9));
					tm.setOriginalTitle(rs.getString(10));
					tm.setStoryline(rs.getString(11));
					tm.setActors(rs.getString(12));
					tm.setImdbRating(rs.getString(13));
					tm.setPosterurl(rs.getString(14));
					listOfTopRatedMovies.add(tm);
			}
			} catch (Exception e) {
				System.out.println("cannot retrieve "+e);
			}
		return listOfTopRatedMovies;
	}
	public List<Favourites> findAllFavourites() {
		List<Favourites> listOfFavourites= new ArrayList<>();
		try {
			
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from favourites");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					Favourites fs= new Favourites();
					fs.setCurrentid(rs.getInt(1));
					fs.setTitle(rs.getString(2));
					fs.setPosterurl(rs.getString(3));
					fs.setYear(rs.getInt(4));
					fs.setReleasedate(rs.getDate(5));
					fs.setId(rs.getInt(6));
					listOfFavourites.add(fs);
			}
			} catch (Exception e) 
		    {
				System.out.println("cannot retrieve "+e);
			}
		return listOfFavourites;
	}
	

}

