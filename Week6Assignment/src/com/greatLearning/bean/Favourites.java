package com.greatLearning.bean;

import java.sql.Date;

public class Favourites {
	private int currentid;
	private String title;
	private String posterurl;
	private int year;
	private Date releasedate;
	private int id;
	public int getCurrentid() {
		return currentid;
	}
	public void setCurrentid(int currentid) {
		this.currentid = currentid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPosterurl() {
		return posterurl;
	}
	public void setPosterurl(String posterurl) {
		this.posterurl = posterurl;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Date getReleasedate() {
		return releasedate;
	}
	public void setReleasedate(Date releasedate) {
		this.releasedate = releasedate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Favourites(int currentid, String title, String posterurl, int year, Date releasedate, int id) {
		super();
		this.currentid = currentid;
		this.title = title;
		this.posterurl = posterurl;
		this.year = year;
		this.releasedate = releasedate;
		this.id = id;
	}
	public Favourites() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Favourites [currentid=" + currentid + ", title=" + title + ", posterurl=" + posterurl + ", year=" + year
				+ ", releasedate=" + releasedate + ", id=" + id + "]";
	}
	

}
