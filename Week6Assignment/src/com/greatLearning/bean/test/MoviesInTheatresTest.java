package com.greatLearning.bean.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import com.greatLearning.bean.MoviesInTheatres;

public class MoviesInTheatresTest {

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		int res=b.getId();
		assertEquals(0,res);
	}

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setId(1);
		assertTrue(true);
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		String res=b.getTitle();
		assertEquals(null,res);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setTitle("Black Panther");
		assertTrue(true);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		int res=b.getYear();
		assertEquals(0,res);
		
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setYear(2018);
		assertTrue(true);
	}

	
	@Test
	public void testGetAverageRating() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		int res=b.getAverageRating();
		assertEquals(0,res);
		
	}

	@Test
	public void testSetAverageRating() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setAverageRating(0);
		assertTrue(true);
		
	}
	@Test
	public void testGetStoryLine() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		String res=b.getStoryline();
		assertEquals(null,res);
		
	}

	@Test
	public void testSetStoryLine() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setStoryline("After the events of Captain America: Civil War, King T'Challa returns home to the reclusive, technologically advanced African nation of Wakanda to serve as his country's new leader. However, T'Challa soon finds that he is challenged for the throne from factions within his own country. When two foes conspire to destroy Wakanda, the hero known as Black Panther must team up with C.I.A. agent Everett K. Ross and members of the Dora Milaje, Wakandan special forces, to prevent Wakanda from being dragged into a world war.   Written by\\nEditor");
		assertTrue(true);
		
	}
	@Test
	public void testGetContentRating() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		String res=b.getContentRating();
		assertEquals(null,res);
		
	}

	@Test
	public void testSetContentRating() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setContentRating("");
		assertTrue(true);
		
	}
	@Test
	public void testGetImdbRating() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		Float res=b.getImdbRating();
		assertEquals(0.0f,res,0.0f);
		
	}

	@Test
	public void testSetReleaseDate() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setImdbRating(7.0f);
		assertTrue(true);
	}
	@Test
	public void testGetDuration() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		String res=b.getDuration();
		assertEquals(null,res);
		
	}
	@Test
	public void testSetDuration() {
		//fail("Not yet implemented");
		MoviesInTheatres b=new MoviesInTheatres();
		b.setDuration("PT134M");
		assertTrue(true);
	}
	
	
	
	

}
