package com.greatLearning.bean.test;

import static org.junit.Assert.*;
import org.junit.Test;

import com.greatLearning.bean.TopRatedIndia;

public class TopRatedIndiaTest {

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		String res=c.getTitle();
		assertEquals(null,res);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setTitle("Anand");
		assertTrue(true);
		
		
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		int res=c.getYear();
		assertEquals(0,res);
		
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setYear(1971);
		assertTrue(true);
		
	}

	@Test
	public void testGetGenres() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		String res=c.getGenres();
		assertEquals(null,res);
		
		
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setGenres("actors,comics");
		assertTrue(true);
	}

	@Test
	public void testGetContentRating() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		String res=c.getContentRating();
		assertEquals(null,res);
		
	}

	@Test
	public void testSetContentRating() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setContentRating("");
		assertTrue(true);
		
	}

	@Test
	public void testGetOriginalTitle() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		String res=c.getOriginalTitle();
		assertEquals(null,res);
		
	}

	@Test
	public void testSetOriginalTitle() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setOriginalTitle("");
		assertTrue(true);
	}

	@Test
	public void testGetStoryline() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		String res=c.getStoryline();
		assertEquals(null,res);
	}

	@Test
	public void testSetStoryline() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setStoryline("A melodramatic tale of a man with a terminal disease. The story begins with Dr Bhaksar winning a literary prize for his book about a patient called Anand. The rest is flashback. Anand, the title character, is suffering from lymphosarcoma of the intestine. He, however appears to be cheerful on the outside and is determined to extract as much pleasure from his remaining lifespan as is possible. Dr. Bhaskar his physician tends to Anand in his last days. After Anand dies we can however still hear his voice which was recorded on a tape. Dr Bhakser writes a book on his patient and wins a literary prize for it                Written by\\nAnonymous");
		assertTrue(true);
		
	}

	@Test
	public void testGetPosterurl() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		String res=c.getPosterurl();
		assertEquals(null,res);
		
	}

	@Test
	public void testSetPosterurl() {
		//fail("Not yet implemented");
		TopRatedIndia c=new TopRatedIndia();
		c.setPosterurl("https://images-na.ssl-images-amazon.com/images/M/MV5BMjE0Mzk3OTk2NF5BMl5BanBnXkFtZTgwMTQ1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg");
		assertTrue(true);
			
	}

}
