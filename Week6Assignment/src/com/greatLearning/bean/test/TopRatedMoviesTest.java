package com.greatLearning.bean.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatLearning.bean.TopRatedMovies;

public class TopRatedMoviesTest {

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		String res=tm.getTitle();
		assertEquals(null,res);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setTitle("Baazigar");
		assertTrue(true);
		
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		int res=tm.getYear();
		assertEquals(0,res);
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setYear(1993);
		assertTrue(true);
	}

	@Test
	public void testGetPoster() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		String res=tm.getPoster();
		assertEquals(null,res);
	}

	@Test
	public void testSetPoster() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setPoster("MV5BMTc3OTE1MDAxNV5BMl5BanBnXkFtZTgwNjM1NDk5NTE@._V1_SY250_CR0,0,187,250_AL_.jpg");
		assertTrue(true);
	}

	@Test
	public void testGetContentRating() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		String res=tm.getContentRating();
		assertEquals(null,res);
	}

	@Test
	public void testSetContentRating() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setContentRating("");
		assertTrue(true);
		
	}

	@Test
	public void testGetDuration() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		String res=tm.getDuration();
		assertEquals(null,res);
	}

	@Test
	public void testSetDuration() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setDuration("PT175M");
		assertTrue(true);
	}
	@Test
	public void testGetAverageRating() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		int res=tm.getAverageRating();
		assertEquals(0,res);
	}

	@Test
	public void testSetAverageRating() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setAverageRating(0);
		assertTrue(true);
		
		
	}
	@Test
	public void testGetOriginalTitle() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		String res=tm.getOriginalTitle();
		assertEquals(null,res);
	}

	@Test
	public void testSetOriginalTitle() {
		//fail("Not yet implemented");
		TopRatedMovies tm=new TopRatedMovies();
		tm.setOriginalTitle("");
		assertTrue(true);
		
	}

}
