package com.greatLearning.resource;
import java.sql.Connection;

import java.sql.DriverManager;

public class DbResource 
{
private static Connection connection;
private DbResource() {
	System.out.println("driver loaded before try block");
	try 
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6Assignment_srinija","root","Srinija123!");
		System.out.println("driver loaded successfully");
	} catch (Exception e) 
	{
	  System.out.println("exception caught due to error in  Db conection "+e);
	}
	}
	public static Connection getDbConnection() {
			try 
			{
				Class.forName("com.mysql.cj.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6Assignment_srinija","root","Srinija123!");
				return connection;
			} catch (Exception e) {
				System.out.println("exception caught"+e);
			}
			return null;
		}
		
		// This method is called from main at the last after all operations finish. 
		public static void closeConnection() {
			try 
			{
				connection.close();
			} 
			catch (Exception e) {
				System.out.println("exception caught because the connection is not closed"+e);
			}
		}
	}

