package com.greatLearning.factory;
import com.greatLearning.factory.Favourites;
import com.greatLearning.factory.TopRatedMovies;
import com.greatLearning.factory.TopRatedIndia;
import com.greatLearning.factory.MoviesInTheatres;
import com.greatLearning.factory.MoviesComing;
import com.greatLearning.factory.Movies;

public class MoviesFactory {
	//use getInstance method to get object of type movies
	public static Movies getInstance(String type) {
		if(type.equalsIgnoreCase("movies-coming")) {
			return  new MoviesComing();
		}else if(type.equalsIgnoreCase("movies-in-theatres")) {
			return  new MoviesInTheatres();
		}else if(type.equalsIgnoreCase("top-rated-india")) {
			return  new TopRatedIndia();
		}else if(type.equalsIgnoreCase("top-rated-movies")) {
            return  new TopRatedMovies();
		}else if(type.equalsIgnoreCase("favourites")) {
			return  (Movies) new Favourites();
		}
		else {
			return null;
		}
	}
	
}

