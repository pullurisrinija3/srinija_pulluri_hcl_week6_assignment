package com.greatlearning.week11.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.week11.bean.Book;
import com.greatlearning.week11.service.BookService;

@RestController
@RequestMapping("/Book")
public class BookController {

	@Autowired
	BookService bookService;
	
	@GetMapping(value = "getAllBooks",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBookDetails()
	{
	return bookService.getAllBooks();
	}
	@PostMapping(value = "storeBookDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBookInfo(@RequestBody Book book) 
	{	
	return bookService.storeBookDetails(book);
	}
	
	@DeleteMapping(value = "deleteBook/{id}")
	public String deleteBookInfo(@PathVariable("id") int BookId)
	{
	return bookService.deleteBooks(BookId);
	}
	
	@PatchMapping(value = "updateBooks")
	public String updateBookInfo(@RequestBody Book book) 
	{
	return bookService.updateBooks(book);
	}

}
