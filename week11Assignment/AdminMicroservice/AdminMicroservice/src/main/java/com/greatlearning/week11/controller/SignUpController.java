package com.greatlearning.week11.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.greatlearning.week11.bean.SignUp;
import com.greatlearning.week11.service.SignUpService;
@RestController
@RequestMapping("/SignUp")
public class SignUpController {
	@Autowired
	SignUpService signupService;
	
	@PostMapping(value = "storeSignUpDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeSignupInfo(@RequestBody SignUp signup) 
	{	
	return signupService.storeSignUpDetails(signup);
	}
	@GetMapping(value = "getAllSignUpDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SignUp> getAllSignUpDetails()
	{
	return signupService.getAllSignUpDetails();
	}
}
