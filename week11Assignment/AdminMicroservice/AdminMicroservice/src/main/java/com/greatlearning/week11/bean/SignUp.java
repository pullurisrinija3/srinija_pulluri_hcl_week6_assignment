package com.greatlearning.week11.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="signup")
public class SignUp {
	@Id
	private int signupid;
	@Column(name="firstname")
	private String firstName;
	@Column(name="lastname")
	private String LastName;
	@Column(name="middlename")
	private String MiddleName;
	private String username;
	private String password;
	public int getSignupid() {
		return signupid;
	}
	public void setSignupid(int signupid) {
		this.signupid = signupid;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "SignUp [signupid=" + signupid + ", firstName=" + firstName + ", LastName=" + LastName + ", MiddleName="
				+ MiddleName + ", username=" + username + ", password=" + password + "]";
	}

}
