package com.greatlearning.week11.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.week11.bean.Users;
import com.greatlearning.week11.dao.UsersDao;
@Service
public class UsersService {
	@Autowired
	UsersDao userDao;
	public String storeUserDetails(Users user)
	{	
		if(userDao.existsById(user.getUserId())) 
		{
		  return "please provide correct details..!!";
		}else {
		  userDao.save(user);
		  return "data stored successfully";
		}
    }
	
	public String deleteUserDetails(int userId)
	{
		if(!userDao.existsById(userId)) {
			return "UserId is not present";
			}else {
			userDao.deleteById(userId);
			return "UserId deleted successfully";
			}	
	}
	
	public String updateUserDetails(Users user) {
		if(!userDao.existsById(user.getUserId())) 
		{
		    return "userId is not present with the given data";
		}
		else 
		{
			Users u	= userDao.getById(user.getUserId()); 
			u.setEmail(user.getEmail());					
			userDao.saveAndFlush(u);				
			return "details updated successfully";
		}	
	}
	
	public List<Users> getAllUsers(){
		return userDao.findAll();
	}
}

