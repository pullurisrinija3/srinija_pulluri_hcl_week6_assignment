package com.greatlearning.week11.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.week11.dao.LoginDao;

@Service
public class LogoutService {
	@Autowired
	LoginDao loginDao;
	public String deleteLoginDetails(int loginid)
	{
		if(!loginDao.existsById(loginid)) {
			return "loginid is not present";
			}else {
			return "Entered loginid logged out successfully";
			}	
	}
}
