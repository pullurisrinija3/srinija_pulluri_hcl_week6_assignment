package com.greatlearning.week11.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.week11.bean.Login;
import com.greatlearning.week11.dao.LoginDao;
@Service
public class LoginService {
	@Autowired
	LoginDao loginDao;
	public String storeLoginDetails(Login login)
	{	
		if(loginDao.existsById(login.getLoginid())) 	
		{
		  return "please try again..!";
		}else {
		  loginDao.save(login);
		  return "login successfull";
		}
    }
	public List<Login> getAllLoginDetails(){
		return loginDao.findAll();
	}
	
}
