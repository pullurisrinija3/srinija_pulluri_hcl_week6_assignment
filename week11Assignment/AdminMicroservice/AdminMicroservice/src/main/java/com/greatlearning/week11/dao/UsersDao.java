package com.greatlearning.week11.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.greatlearning.week11.bean.Users;
@Repository
public interface UsersDao extends JpaRepository<Users,Integer>{
}
