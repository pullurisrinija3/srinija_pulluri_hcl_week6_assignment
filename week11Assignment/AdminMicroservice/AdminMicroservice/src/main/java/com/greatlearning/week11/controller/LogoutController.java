package com.greatlearning.week11.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.greatlearning.week11.service.LogoutService;

@RestController
@RequestMapping("/Login")
public class LogoutController {
	@Autowired
	LogoutService logoutService;
	@GetMapping(value = "Logout/{loginid}")
	public String deleteLoginInfo(@PathVariable("loginid") int loginid)
	{
	return logoutService.deleteLoginDetails(loginid);
	}

}
