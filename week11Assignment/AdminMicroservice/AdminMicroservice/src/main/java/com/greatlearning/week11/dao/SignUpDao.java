package com.greatlearning.week11.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.week11.bean.SignUp;
@Repository
public interface SignUpDao extends JpaRepository<SignUp,Integer>{

}
