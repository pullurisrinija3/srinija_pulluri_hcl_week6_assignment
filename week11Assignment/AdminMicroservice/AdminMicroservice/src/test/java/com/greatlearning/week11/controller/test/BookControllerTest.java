package com.greatlearning.week11.controller.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.greatlearning.week11.bean.Book;
import com.greatlearning.week11.controller.BookController;
import com.greatlearning.week11.dao.BookDao;
import com.greatlearning.week11.service.BookService;
@SpringBootTest
class BookControllerTest {
	//String baseUrl="http://localhost:9090/Book";
	@Autowired
	BookController bc;
	@Mock
	BookDao dao;
	@InjectMocks
	BookService service;
	@Test
	void testGetAllBookDetails() {
		//fail("Not yet implemented");
		List<Book> list=bc.getAllBookDetails();
		assertTrue(true);
	}

	@Test
	void testStoreBookInfo() {
		//fail("Not yet implemented");
//		RestTemplate resttemplate=new RestTemplate();
//		Book b=new Book();
//		b.setBookAuthor("Shakespeare");
//		b.setBookName("you can win");
//		b.setBookPrice(7809);
//		b.setBookType("fiction");
//		String res	= resttemplate.postForObject(baseUrl+"/storeBookDetails", b, String.class);
//		assertEquals(res, "id must be unique");
		Book b=new Book();
		b.getBookAuthor();
		b.getBookName();
		b.getBookPrice();
		b.getBookType();
		b.getId();
	    String res=bc.storeBookInfo(b);
	    assertTrue(true);
	}

	@Test
	void testDeleteBookInfo() {
		//fail("Not yet implemented");
		Book b=new Book();
		b.getId();
		String res=bc.deleteBookInfo(1);
		assertTrue(true);
	}

	@Test
	void testUpdateBookInfo() {
		//fail("Not yet implemented");
		Book b = new Book();
		when(dao.findById(b.getId())).thenReturn(Optional.of(b));
		service.updateBooks(b);
		Book b1 = new Book();
		assertEquals(0,b1.getBookPrice());
	}

}
