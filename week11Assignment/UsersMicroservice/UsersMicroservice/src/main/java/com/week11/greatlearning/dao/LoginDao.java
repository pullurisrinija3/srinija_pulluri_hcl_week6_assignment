package com.week11.greatlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.week11.greatlearning.bean.Login;

@Repository
public class LoginDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	//private String driver="com.mysql.cj.jdbc.Driver";
//	public void ConnectiontoDB(String driver) {
//		try {
//			Class.forName(driver);
//			
//			
//		}catch(Exception e) {
//			System.out.println("exception caught"+e);
//		}
//	}public Connection getConnection() {
//		Connection con=null;
//		try {
//			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/week7Assignment_Srinija","root","Srinija123!");
//		}catch(Exception e) {
//			System.out.println(e);
//		}
//		return con;
//	}
//	public int StoreLoginDetails(Login login)
//	{
//		ConnectiontoDB(driver);
//		Connection con=getConnection();
//		try
//		{
//			return jdbcTemplate.update("insert into Login values(?,?,?)", login.getEmailID(),login.getUsername(),login.getPassword());
//		}
//		catch(Exception e)
//		{
//			System.out.println("store method exception"+e);
//			return 0;
//		  	
//		}
//	}
	public Login UserLogin(Login login) {
      return  (Login) jdbcTemplate.queryForObject("select EmailID,username,password from Login where EmailID=?,username=? and password=?", new Object[] {login.getEmailID(),login.getUsername(),login.getPassword()}, new LoginRowMapper());
	}
}
 class LoginRowMapper implements RowMapper {
		@Override
		public Login mapRow(ResultSet rs, int rowNum) throws SQLException  {
			Login login = new Login();
			login.setEmailID(rs.getString("EmailID"));
			login.setUsername(rs.getString("username"));
			login.setPassword(rs.getString("password"));
			return login;
		}
}