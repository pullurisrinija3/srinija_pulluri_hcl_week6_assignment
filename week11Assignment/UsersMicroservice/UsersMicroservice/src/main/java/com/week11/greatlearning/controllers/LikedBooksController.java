package com.week11.greatlearning.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.week11.greatlearning.service.BookService;

@Controller
public class LikedBooksController {

	@Autowired
	BookService likeservice;
	@PostMapping(value="LikedBooks.jsp")
	public void LikedBooks(@RequestParam("BookId")String BookId, HttpServletRequest req, HttpSession hs, HttpServletResponse hp)
	{	
    	String param=BookId;
		List list;
		if(hs.getAttribute("addToLikedBooks")==null)
		{
			list=new ArrayList();
			list.add(param);
			hs.setAttribute("addToLikedBooks", list);
		}else
		{
			list=(List) hs.getAttribute("addToLikedBooks");
			if(!list.contains(param)){
			list.add(param);
	
			hs.setAttribute("addToLikedBooks", list);
			
		}
		    try {
		    hp.getWriter().print(list.size());
		    } catch (IOException e) {
					e.printStackTrace();
				}
		
		}
		}		

}
