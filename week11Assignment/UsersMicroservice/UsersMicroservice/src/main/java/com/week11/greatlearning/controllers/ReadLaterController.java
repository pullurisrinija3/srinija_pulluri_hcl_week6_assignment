package com.week11.greatlearning.controllers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import com.week11.greatlearning.service.BookService;

@Controller
public class ReadLaterController {
	@Autowired
	BookService bookservice; 
//	@GetMapping(value = "ReadLaterBooks")
//	public ModelAndView likeProduct(HttpServletRequest req) {
//		ModelAndView mav = new ModelAndView();
//		int BookId = Integer.parseInt(req.getParameter("BookId"));
//		Book listOfBooks = bookservice.getBookbyBookId(BookId);
//		req.setAttribute("books", listOfBooks);
//		req.setAttribute("obj", "");
//		mav.setViewName("ReadLaterBooks.jsp");
//		return mav;
//	}
	@PostMapping(value="ReadLaterBooks")
	public void userLogin(HttpServletRequest req, HttpSession hs, HttpServletResponse hp) throws IOException{
		String param=req.getParameter("BookId");
		List list;
		if(hs.getAttribute("readLaterBooks")==null)
		{
			list=new ArrayList();
			list.add(param);
			hs.setAttribute("readLaterBooks", list);
		}else
		{
			list=(List) hs.getAttribute("readLaterBooks");
			if(!list.contains(param)){
			list.add(param);
		}
			hs.setAttribute("readLaterBooks", list);
			
		}
     		hp.getWriter().print(list.size());
		
		}
}
