package com.week11.greatlearning.controllers;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.week11.greatlearning.service.BookService;
import com.week11.greatlearning.service.LoginService;

public class LoginController {
	@Autowired
	LoginService loginservice;
	@Autowired
	BookService bookservice;
//	@RequestMapping(value = "Login",method = RequestMethod.GET)
//	public ModelAndView storeLoginDetails(HttpServletRequest req,HttpSession hs) {	 
//		hs.setAttribute("EmailID", hs);
//		hs.setAttribute("username",hs);
//		hs.setAttribute("password", hs);
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("Login.jsp");
//		return mav;
//	}
	@RequestMapping("/Login")  
	public String login()
	{
		return "Login";
	}

	@PostMapping("/Login")
	public ModelAndView validateUser(@RequestParam("EmailID") String EmailID,@RequestParam("username") String username,@RequestParam("password") String password) {

		if (loginservice.validateUser(EmailID,username, password))
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("displayDashboard");
			mv.addObject("books",bookservice.getAllBooks());
			System.out.println(bookservice.getAllBooks());
	        return mv; 
		}
		else {
			ModelAndView mv = new ModelAndView();
			mv.setViewName("Login");
			return mv;
		}
	}
	
}
