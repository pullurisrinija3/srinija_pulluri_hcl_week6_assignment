package com.week11.greatlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.week11.greatlearning.bean.Book;

@Repository
public class BookDao {
@Autowired
JdbcTemplate jdbctemplate;
 public List<Book> retrieveBookDetails() {
     try {
     	return jdbctemplate.query("select * from Book", (rs,rowNum)-> {
		Book book=new Book();
		book.setBookId(rs.getInt(1));
		book.setBookName(rs.getString(2));
		book.setBookType(rs.getString(3));
		book.setBookAuthor(rs.getString(4));
		book.setBookImage(rs.getString(5));
		book.setBookPrice(rs.getFloat(6));
		return book;
	});
}catch (Exception e) {
		System.out.println(e);
	}
	return null;
}
class MyRowMapper implements RowMapper<Book>{
	@Override
	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		Book book =new Book();
		book.setBookId(rs.getInt(1));
		book.setBookName(rs.getString(2));
		book.setBookType(rs.getString(3));
		book.setBookAuthor(rs.getString(4));
		book.setBookImage(rs.getString(5));
		book.setBookPrice(rs.getFloat(6));
		return book;
	}
}
public Book getBookByBookId(int BookId){    
    String sql="select * from Book where BookId=?";    
    return jdbctemplate.queryForObject(sql, new Object[]{BookId},new BeanPropertyRowMapper<Book>(Book.class));    
}
public String addtoLikedBooks(Book book){
	String sql = "insert into LikedBook  values(?,?,?,?,?,?)";
	int res = jdbctemplate.update(sql, book);
	if (res!=0)
		return "success";
	else
		return "try again..!!!";
}
}

