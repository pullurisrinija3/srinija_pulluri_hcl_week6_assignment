package com.week11.greatlearning.dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
@Repository
public class SignupDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
    public String registerUser(String firstName,String LastName,String MiddleName,String username, String password,String retypePassword){
		String sql = "insert into Signup values (?,?,?,?,?,?)";
		 if(jdbcTemplate.update(sql, firstName,LastName,MiddleName,username, password,retypePassword)!=0)
			return "registration successfull!";
		else
			return "Username Already Exists!!!";		
	}
}
