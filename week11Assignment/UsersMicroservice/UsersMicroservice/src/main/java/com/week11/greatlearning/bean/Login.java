package com.week11.greatlearning.bean;

public class Login {
	private String EmailID;
	private String username;
	private String password;
	public String getEmailID() {
		return EmailID;
	}
	public void setEmailID(String emailID) {
		this.EmailID = emailID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Login(String emailID, String username, String password) {
		super();
		EmailID = emailID;
		this.username = username;
		this.password = password;
	}
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Login [EmailID=" + EmailID + ", username=" + username + ", password=" + password + "]";
	}
	

}
