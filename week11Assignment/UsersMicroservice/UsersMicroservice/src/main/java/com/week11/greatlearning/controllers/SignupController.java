package com.week11.greatlearning.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.week11.greatlearning.service.SignupService;
@Controller
public class SignupController {
	@Autowired
	SignupService signupservice;
	@RequestMapping(value = "Signup",method = RequestMethod.GET)
	public ModelAndView storeLoginDetails(HttpServletRequest req,HttpSession hs) {	 
		hs.setAttribute("firstName", hs);
		hs.setAttribute("LastName",hs);
		hs.setAttribute("MiddleName",hs);
		hs.setAttribute("username",hs);
		hs.setAttribute("password", hs);
		hs.setAttribute("retypePassword",hs);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("Signup.jsp");
		return mav;
	}
//	@PostMapping("/Signup")
//	public ModelAndView registerUser(@RequestParam("MiddleName") String MiddleName,@RequestParam("LastName") String LastName,@RequestParam("firstName") String firstName,@RequestParam("username") String username, @RequestParam("password") String password,@RequestParam("retypePassword") String retypePassword) {
//
//		try {
//			signupservice.registerUser(firstName,LastName,MiddleName,username, password,retypePassword);
//			ModelAndView mv = new ModelAndView();
//			mv.setViewName("Signup");
//			mv.addObject("username",username);
//			return mv;
//		} catch (DuplicateKeyException e) {
//			ModelAndView mv = new ModelAndView();
//			mv.setViewName("registerError");
//			return mv;
//		}

}
