create database week9Assignment_Srinija;

use week9Assignment_Srinija;

create table Book(id int primary key,BookName varchar(60),BookType varchar(60),BookAuthor varchar(60),BookImage varchar(250),BookPrice float);

insert into Book values(1,"The Everyday Hero Manifesto","Economics","Robin sharma","https://i.harperapps.com/hcanz/covers/9780008312879/x480.jpg",450.00);

insert into Book values(2,"Think like a monk","Stress management","Jay shetty","https://th.bing.com/th/id/OIP.roIPRdZHY44oMB_pMBDx3AHaLJ?w=115&h=180&c=7&r=0&o=5&dpr=1.5&pid=1.7",300.03);

insert into Book values(3,"julius caesar","Literature","William shakespeare","https://s3.ap-south-1.amazonaws.com/storage.commonfolks.in/docs/products/images_full/julius-caesar-shakespeare-s-greatest-stories-for-children_FrontImage_787.jpg",650);

insert into Book values(4,"Disney mickey mouse club house","comics","grace baranowski","https://images-na.ssl-images-amazon.com/images/I/51Ss0HI+PHL._SX490_BO1,204,203,200_.jpg",65.00);

insert into Book values(5,"hookers and blow","comics","Munty C. Pepin","https://m.media-amazon.com/images/I/81GY6msTP2L._AC_UY327_FMwebp_QL65_.jpg",65.98);

insert into Book values(6,"The hundred gifts","fiction","Jennifer scott","https://th.bing.com/th/id/OIP.jGfgAFGYB5IpyVAGtdga2QAAAA?w=115&h=180&c=7&r=0&o=5&dpr=1.5&pid=1.7",750.00);

insert into Book values(7,"Rush on the radio","Arts & photography","James Golden","https://images-na.ssl-images-amazon.com/images/I/4193I4qLvfL._SX332_BO1,204,203,200_.jpg",50.00);

insert into Book values(8,"Avatar:The Last Airbender","Arts & photography","Jenny dorsey","https://images-na.ssl-images-amazon.com/images/I/61qwFOWdNAS._SX400_BO1,204,203,200_.jpg",150.00);

insert into Book values(9,"Marvel By Design ","Drawing","gestalten","https://images-na.ssl-images-amazon.com/images/I/51JovABgzVL._SX339_BO1,204,203,200_.jpg",234.00);

insert into Book values(10,"The Queen: 70 years of Majestic Style","fashion","Bethan holt","https://images-na.ssl-images-amazon.com/images/I/411pJpTvdqS._SX405_BO1,204,203,200_.jpg",320.00);

insert into Book values(11,"HR Giger. 40th Ed.","History & Criticism","Andreas J. Hirsch","https://images-na.ssl-images-amazon.com/images/I/51R7xSyotQL._SX370_BO1,204,203,200_.jpg",430.00);

insert into Book values(12,"BTS CALENDAR 2022","music","BTS Official","https://images-na.ssl-images-amazon.com/images/I/41W2ODM020L._SY498_BO1,204,203,200_.jpg",630.00);

insert into Book values(13,"The Art of Halo Infinite","Computer & technology","Microsoft","https://images-na.ssl-images-amazon.com/images/I/511tEyCIqhL._SX377_BO1,204,203,200_.jpg",870.00);

insert into Book values(14,"Windows 11 for dummies","Computer & technology","Andy Rathbone","https://images-na.ssl-images-amazon.com/images/I/51ryB-txJJL._SX397_BO1,204,203,200_.jpg",2370.00);

insert into Book values(15,"The Judge's List: A Novel (The Whistler)","Thrillers & suspense","John Grisham","https://images-na.ssl-images-amazon.com/images/I/416Uc0RhQWL._SX327_BO1,204,203,200_.jpg",1270.00);

select * from Book;

create table Login(loginid int primary key,EmailID varchar(50),username varchar(30),password char(32));

select * from Login;

create table SignUp(signupid int primary key,firstName varchar(30),LastName varchar(50),MiddleName varchar(40),username varchar(40),password varchar(32));

select * from SignUp;

create table Users(userId int primary key,username varchar(60),Email varchar(50),password varchar(50),retypePassword varchar(60));

select * from Users;

create table likedBooks(id int primary key,BookName varchar(60),BookType varchar(60),BookAuthor varchar(60),BookImage varchar(250),BookPrice float);

select * from likedBooks;









