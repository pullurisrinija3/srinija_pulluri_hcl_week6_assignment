package com.miniproject.sr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.miniproject.sr.pojo.AdminLogin;
import com.miniproject.sr.pojo.Items;
import com.miniproject.sr.pojo.LoginUser;
import com.miniproject.sr.repository.AdminLogRepository;
import com.miniproject.sr.repository.ItemRepository;
import com.miniproject.sr.repository.UserRepository;

@SpringBootApplication
public class MiniprojectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(MiniprojectApplication.class, args);
	}
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemRepository bookRepository;
	
	@Autowired
	private AdminLogRepository adminLogRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser login1=new LoginUser("Vinoosha","RS","vinoosha@gmail.com","9900990099","vinoosha","123456");
		userRepository.save(login1);
		
		LoginUser login2=new LoginUser("Akhil","RS","akhil@gmail.com","9999000000","akhil","123456");
		userRepository.save(login2);
		
		LoginUser login3=new LoginUser("Sam","Y","sam@gmail.com","8888800000","goa","goa");
		userRepository.save(login3);
		
		LoginUser login4=new LoginUser("Nikhil","T","nikhil@test.com","7777788888","nikhil","nikhil");
		userRepository.save(login4);
		
		AdminLogin admin1=new AdminLogin("tharun", "rr", "tharun@gmail.com", "9999911111", "tarun", "tarun");
		adminLogRepository.save(admin1);

		Items book=new Items("Veg Roll","80","3","IMG001");
		bookRepository.save(book);
		
		Items book1=new Items("Burgur","100","1","IMG002");
		bookRepository.save(book1);
		
		Items book2=new Items("Samosa","20","1","IMG003");
		bookRepository.save(book2);
		
		Items book3=new Items("South India Meal ","150","3","IMG004");
		bookRepository.save(book3);
		
		Items book4=new Items("Parata","50","2","IMG005");
		bookRepository.save(book4);
		
		
	}


}
