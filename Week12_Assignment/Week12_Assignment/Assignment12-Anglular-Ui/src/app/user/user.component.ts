import { Component, OnInit } from '@angular/core';
import { User , loginUser} from '../user';
import { Book } from '../book';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  books:Array<Book>=[];
  storemsg:string="";
  users= new  loginUser();
  msg:string="";
  public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
  constructor(public pser:UserService, public  router:Router)  { }

  ngOnInit(): void {
   this.loadProducts();
  }
  loadProducts(): void{
    // console.log("event fired")
    this.pser.loadProductDetails().subscribe(res=>this.books=res);
   // this.pser.loadProductDetails();
   }
storeProduct(productRef:NgForm)
{
  //console.log(productRef.value)
  this.pser.storeProductDetails(productRef.value).subscribe(res=>this.storemsg=res,error=>console.log(error));
  
}
loginUser()
{
this.pser.loginUserFromRemote(this.users).subscribe(
  data =>{ 
    this.router.navigate(['/welcome'])  
   },
  error => {console.log("exception occured");
this.msg="Bad credentials,please add proper credentials";
}
)
}
Hide()
{
  this.hide=!this.hide;
  if(this.hide)
  {
    
    this.nameT= "Hide";

  }
  else{
    this.nameT = "Show";
  }
}
}
