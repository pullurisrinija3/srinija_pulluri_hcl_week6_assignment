package com.greateleaning.assignment9.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greateleaning.assignment9.bean.Admin;
import com.greateleaning.assignment9.service.AdminService;

@RestController
@RequestMapping("/admins")
@EntityScan(basePackages = "com.greateleaning.assignment9.bean")
@CrossOrigin
public class AdminController {
@Autowired
AdminService adminService;


@PostMapping(value="addAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
public String insertAdmin(@RequestBody Admin admin)
{
	return  adminService.addAdminINfo(admin);
}



@PostMapping(value="login",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String adminDetails(@RequestBody Admin admin)	
{
Admin find= adminService.login(admin.getEmail(), admin.getPassword());
	
	if(Objects.nonNull(find))	
	{
		
		return "login Successfully";
	}
	else
	{
		
		return "login failed"; 
	}
}
@PostMapping(value="logout",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String adminDetails(HttpSession hs)
{
	 hs.invalidate();
	return "logout Sussefully";
}
}
