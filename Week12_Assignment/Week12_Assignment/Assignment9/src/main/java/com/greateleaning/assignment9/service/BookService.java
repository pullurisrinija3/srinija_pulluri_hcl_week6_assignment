package com.greateleaning.assignment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greateleaning.assignment9.bean.Book;
import com.greateleaning.assignment9.dao.BookDao;

@Service
public class BookService {
	@Autowired
	BookDao bookDao;
	public String addBookINfo(Book book)
	{
		if(bookDao.existsById(book.getBookid()))
		{
			return "id  not present";
		}
		else
		{
			bookDao.save(book);
			return " store";
		}
	}
	public List<Book> getAllBooks()
	{
		return bookDao.findAll();
	}
	
	public String deleteBook(int bookid)
	{
		if(!bookDao.existsById(bookid))
		{
			return "id  not present";
		}
		else
		{
		bookDao.deleteById(bookid);;
			return "deleted";
		}

	}
	public String updateBook(Book book)
	{
		if(!bookDao.existsById(book.getBookid()))
		{
			return "data Store";
		}
		else
		{
	Book p=bookDao.getById(book.getBookid());
	p.setImage(book.getImage());
	p.setAuthorname(book.getAuthorname());
	p.setBooktype(book.getBooktype());
	p.setBookname(book.getBookname());
      bookDao.saveAndFlush(p);
			return "updated ";
		}	
	}
	
	

}
